# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: ovirchen <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/10/25 12:45:37 by ovirchen          #+#    #+#              #
#    Updated: 2017/10/25 12:45:38 by ovirchen         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libftprintf.a

SRCS = 	ft_strnew.c \
		ft_strlen.c \
		ft_printf.c \
		ft_itoa_base.c \
		ft_strjoin.c \
		ft_strcpy.c \
		ft_strcat.c \
		ft_strdup.c \
		ft_pointer.c \
		ft_string.c \
		ft_char.c \
		ft_number.c \
		flags.c \
		ft_bzero.c \
		join.c \

OBJ = $(SRCS:%.c=%.o)

HEADERS = libftprintf.h

FLAGS = -Wall -Wextra -Werror

all: $(NAME)

$(NAME): $(OBJ)
	@ar rc $(NAME) $(OBJ)
	@ranlib $(NAME)

$(OBJ): $(SRCS)
	@gcc $(FLAGS) -c -I $(HEADERS) $(SRCS)

clean:
	@rm -f $(OBJ)

fclean: clean
	@rm -f $(NAME)

re: fclean all